/*
MySQL Backup
Database: food_recipe
Backup Time: 2024-07-08 14:59:54
*/

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `food_recipe`.`categories`;
DROP TABLE IF EXISTS `food_recipe`.`levels`;
DROP TABLE IF EXISTS `food_recipe`.`recipes`;
DROP TABLE IF EXISTS `food_recipe`.`users`;
CREATE TABLE `categories` (
  `category_id` int NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `created_time` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `modified_time` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
CREATE TABLE `levels` (
  `level_id` int NOT NULL AUTO_INCREMENT,
  `created_time` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `level_name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `modified_time` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
CREATE TABLE `recipes` (
  `recipe_id` int NOT NULL AUTO_INCREMENT,
  `created_time` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `how_to_cook` text COLLATE utf8mb4_general_ci,
  `image_filename` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `ingredient` text COLLATE utf8mb4_general_ci,
  `modified_time` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `recipe_name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `time_cook` int DEFAULT NULL,
  `category_id` int DEFAULT NULL,
  `level_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`recipe_id`),
  KEY `category_id` (`category_id`),
  KEY `level_id` (`level_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `recipes_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`),
  CONSTRAINT `recipes_ibfk_2` FOREIGN KEY (`level_id`) REFERENCES `levels` (`level_id`),
  CONSTRAINT `recipes_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
CREATE TABLE `users` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `password` text COLLATE utf8mb4_general_ci,
  `fullname` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `role` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `created_time` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `modified_time` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
BEGIN;
LOCK TABLES `food_recipe`.`categories` WRITE;
DELETE FROM `food_recipe`.`categories`;
INSERT INTO `food_recipe`.`categories` (`category_id`,`category_name`,`created_time`,`modified_time`) VALUES (1, 'Lunch', '2024-03-04 10:08:35.000000', '2024-03-04 10:08:35.000000'),(2, 'Breakfast', '2024-03-04 10:08:35.000000', '2024-03-04 10:08:35.000000'),(3, 'Dinner', '2024-03-04 10:08:35.000000', '2024-03-04 10:08:35.000000'),(4, 'Snack', '2024-03-04 10:08:35.000000', '2024-03-04 10:08:35.000000')
;
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `food_recipe`.`levels` WRITE;
DELETE FROM `food_recipe`.`levels`;
INSERT INTO `food_recipe`.`levels` (`level_id`,`created_time`,`level_name`,`modified_time`) VALUES (1, '2024-03-04 10:08:35.000000', 'Master Chef', '2024-03-04 10:08:35.000000'),(2, '2024-03-04 10:08:35.000000', 'Hard', '2024-03-04 10:08:35.000000'),(3, '2024-03-04 10:08:35.000000', 'Medium', '2024-03-04 10:08:35.000000'),(4, '2024-03-04 10:08:35.000000', 'Easy', '2024-03-04 10:08:35.000000')
;
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `food_recipe`.`recipes` WRITE;
DELETE FROM `food_recipe`.`recipes`;
INSERT INTO `food_recipe`.`recipes` (`recipe_id`,`created_time`,`how_to_cook`,`image_filename`,`ingredient`,`modified_time`,`recipe_name`,`time_cook`,`category_id`,`level_id`,`user_id`) VALUES (2, '2024-03-21 10:08:35.000000', 'campur susu cair dan air lemon. Diamkan 3 menit Campur semua bahan kering, aduk rata', 'https://www.allrecipes.com/thmb/imrP1HYi5pu7j1en1_TI-Kcnzt4=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/20513-classic-waffles-mfs-025-4x3-81c0f0ace44d480ca69dd5f2c949731a.jpg', 'tepung terigu serbaguna', '2024-03-21 10:08:35.000000', 'Es Blewah cendol dawet agar-agar', 20, 2, 4, 1),(3, '2024-03-21 10:08:35.000000', 'Rebus telur, kentang dan wortel', 'https://www.culinaryhill.com/wp-content/uploads/2022/05/Egg-Salad-Culinary-Hill-1200x800-1.jpg', 'telur bebek rebus', '2024-03-21 10:08:35.000000', 'Egg Salad', 30, 1, 3, 1),(4, '2024-03-21 10:08:35.000000', 'Tepung Pisang Goreng Gluten Free Ladang Lima, bubuk minuman coklat dan margarin.', 'https://www.imperialsugar.com/sites/default/files/recipe/Chocolate-Pancakes-imperial.jpg', '200 gram tepung pisang goreng Ladang Lima', '2024-03-21 10:08:35.000000', 'Chocola Pancake', 40, 4, 3, 1),(5, '2024-03-21 10:08:35.000000', 'Kocok lepas telur, tambahkan garam, merica, kaldu bubuk secukupnya, kocok rata.', 'https://img.buzzfeed.com/thumbnailer-prod-us-east-1/d1cb1ef69a1c4b6c9a10e1662082f0e7/BFV77395_HowToCookAPerfectOmelet_ADB_032221_V06l_16x9_YT.jpg?resize=1200:*&output-format=jpg&output-quality=auto', '3 butir telur', '2024-03-21 10:08:35.000000', 'Omelette rush', 45, 3, 2, 1),(6, '2024-03-28 11:37:25.701695', '<ul><li>Cuci</li><li>Rebus</li><li>Beri sedikit garam</li></ul>', 'https://umsu.ac.id/health/wp-content/uploads/2024/02/memahami-manfaat-luar-biasa-pakcoy-untuk-kesehatan.jpg', '<ul><li>Pakcoy</li><li>Garam</li></ul>', '2024-03-28 11:37:25.701695', 'Pakcoy', 10, 4, 4, 1),(7, '2024-03-28 11:40:00.891785', '', 'https://www.foodandwine.com/thmb/DI29Houjc_ccAtFKly0BbVsusHc=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/crispy-comte-cheesburgers-FT-RECIPE0921-6166c6552b7148e8a8561f7765ddf20b.jpg', '', '2024-03-28 11:40:00.891785', 'Jamur', NULL, 2, 1, 1),(8, '2024-04-01 11:40:34.324635', '<ol><li>Siapkan roti, olesi roti dg margarin</li><li>Siapkan grill/teflon utk memanggang beef dan roti</li><li>Potong tipis² timun dan tomat. Dan bersih kan selada</li><li>Bahan saos : campurkan semua bahan saos jd satu dlm wadah</li><li>Setelah beef dan roti matang, siapkan di wadah, tata, mulai dari roti bagian bawah, lalu beri selada, beef, bahan saos, timun dan tomat, terakhir tut</li><li>Dirasa kurang pedas boleh ditambah dg saos lagi</li><li>Beef burger siap di nikmati</li></ol>', 'https://www.foodandwine.com/thmb/DI29Houjc_ccAtFKly0BbVsusHc=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/crispy-comte-cheesburgers-FT-RECIPE0921-6166c6552b7148e8a8561f7765ddf20b.jpg', '<ol><li>🍔&nbsp;<a href=https://cookpad.com/id/cari/Roti rel=noopener noreferrer target=_blank style=color: inherit;>Roti</a>&nbsp;burger</li><li><a href=https://cookpad.com/id/cari/Beef rel=noopener noreferrer target=_blank style=color: inherit;>Beef</a>&nbsp;burger</li><li>🥒<a href=https://cookpad.com/id/cari/Timun rel=noopener noreferrer target=_blank style=color: inherit;>Timun</a></li><li><a href=https://cookpad.com/id/cari/Roti rel=noopener noreferrer target=_blank style=color: inherit;>🍅&nbsp;</a><a href=https://cookpad.com/id/cari/Tomat rel=noopener noreferrer target=_blank style=color: inherit;>Tomat</a></li><li><a href=https://cookpad.com/id/cari/Selada rel=noopener noreferrer target=_blank style=color: inherit;>Selada</a></li><li><a href=https://cookpad.com/id/cari/Margarine rel=noopener noreferrer target=_blank style=color: inherit;>Margarine</a></li></ol><p>Bahan saos</p><ol><li>Mayones</li><li><a href=https://cookpad.com/id/cari/Saos%20tomat rel=noopener noreferrer target=_blank style=color: inherit;>Saos tomat</a>&nbsp;(secukupnya)</li><li><a href=https://cookpad.com/id/cari/Saos%20sambal rel=noopener noreferrer target=_blank style=color: inherit;>Saos sambal</a>&nbsp;(secukupnya)</li></ol>', '2024-04-01 11:40:34.324635', 'Beef Burger', 65, 1, 3, 1),(9, '2024-04-01 15:22:14.724293', '1. Masak nasi; 2. Tunggu beberapa menit', 'https://asset.kompas.com/crops/Kyp-MBp3Kf0PLGveth_zzhU2gfI=/0x0:1000x667/750x500/data/photo/2020/07/11/5f09e008e7fee.jpg', 'Nasi; Air; Daun salam; bawang', '2024-04-01 15:22:14.724293', 'Bubur Ayam', 60, 2, 4, 11),(10, '2024-07-02 07:04:22.000000', NULL, NULL, NULL, '2024-07-02 07:04:22.000000', 'Cihuyy', NULL, 1, 2, NULL),(11, '2024-07-02 07:28:10.000000', NULL, NULL, NULL, '2024-07-02 07:28:10.000000', 'Fanum', NULL, 1, 2, NULL),(12, '2024-07-02 07:29:29.000000', NULL, NULL, NULL, '2024-07-02 07:29:29.000000', 'GYATT', NULL, 3, 4, NULL),(13, '2024-07-02 07:43:13.000000', NULL, 'public/recipe-images/W5eSsdGtBaZETt154TmatpluM6uP5ITnnvlj1xC8.jpg', NULL, '2024-07-02 07:43:13.000000', 'Ashepss', NULL, 2, 2, NULL),(14, '2024-07-02 08:42:56.000000', '1. Masak nasi; 2. Tunggu beberapa menit', 'TbX5p3xT4d0shHqrz7FSZTYN9GZf8suFsDU1NhWY.jpg', 'Nasi; Air; Daun salam; bawang', '2024-07-03 04:19:45.000000', 'Abrakabarja', 60, 1, 2, 11),(23, '2024-07-03 08:02:22.000000', 'asdasd', 'KeOleitxjEzmBiocG6i9OTgvNd706i6zsPZP6EtB.jpg', NULL, '2024-07-03 08:02:22.000000', 'Basaasd', 5, 2, 4, NULL),(24, '2024-07-03 09:20:58.000000', '<p>asdasd\'asd\'asd\'</p><p>asd</p><p>asd</p><p>asd</p><p>sad</p><p>a</p><p>SD</p>', 'FQh72K33LdD5n97QH6re6mHCiXz4oZMLs1TDSP60.jpg', '<p>asasasdasdasdasdasdadsas</p><p><br></p><p>ASasd</p><p>asd</p><p>ASD</p><p>SDA</p><p>AS</p><p><br></p>', '2024-07-03 09:20:58.000000', 'dasdasdasd', 11, 2, 1, NULL),(25, '2024-07-03 09:25:11.000000', '<p>DSSASasd</p><p>ASD</p><p>d</p><p>s</p><p>a</p><p>as</p><p><br></p><p>a</p><p>asd</p><p><br></p><p>AS</p>', 'thMDxJG57X65z50n2UaYXAgFzHRhiO2ALsz4ZLks.jpg', '<p>asdasdasdasdasdasdasdas</p><p>asd</p><p>sad</p><p>sad</p><p>sad</p><p>ASS</p><p>aS</p><p><br></p><p>aasd</p>', '2024-07-03 09:25:11.000000', 'asdasdasd', 11, 2, 2, 11),(26, '2024-07-03 09:30:18.000000', NULL, '227U6f0jU0vKAu3ANzkpUSEGlPeUDL9cHn8ruzyy.png', NULL, '2024-07-03 09:52:48.000000', 'Telur Ceplok Crispy', 15, 1, 4, 11)
;
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `food_recipe`.`users` WRITE;
DELETE FROM `food_recipe`.`users`;
INSERT INTO `food_recipe`.`users` (`user_id`,`username`,`password`,`fullname`,`role`,`created_time`,`modified_time`) VALUES (1, 'user1', '$2b$10$9PQ4l3CIhGck3nWCRmEYieQwNCzVw9j7UW86TybdQFe.1Ztwi3G1C', 'Jane Smith', 'User', '2024-03-04 10:08:35.000000', '2024-03-04 10:08:35.000000'),(2, 'user2', '$2b$10$9PQ4l3CIhGck3nWCRmEYieQwNCzVw9j7UW86TybdQFe.1Ztwi3G1C', 'Ashley Johnson', 'User', '2024-03-04 10:08:35.000000', '2024-03-04 10:08:35.000000'),(3, 'user3', '$2b$10$9PQ4l3CIhGck3nWCRmEYieQwNCzVw9j7UW86TybdQFe.1Ztwi3G1C', 'Bob Brown', 'Admin', '2024-03-04 10:08:35.000000', '2024-03-04 10:08:35.000000'),(4, 'user4', '$2b$10$9PQ4l3CIhGck3nWCRmEYieQwNCzVw9j7UW86TybdQFe.1Ztwi3G1C', 'John Doe', 'Admin', '2024-03-04 10:08:35.000000', '2024-03-04 10:08:35.000000'),(5, 'user5', '$2b$10$9PQ4l3CIhGck3nWCRmEYieQwNCzVw9j7UW86TybdQFe.1Ztwi3G1C', 'Jane Smith', 'User', '2024-03-04 10:08:35.000000', '2024-03-04 10:08:35.000000'),(6, 'user6', '$2b$10$9PQ4l3CIhGck3nWCRmEYieQwNCzVw9j7UW86TybdQFe.1Ztwi3G1C', 'Kukun Kurniawan', 'Admin', '2024-03-04 10:08:35.000000', '2024-03-04 10:08:35.000000'),(7, 'user7', '$2b$10$9PQ4l3CIhGck3nWCRmEYieQwNCzVw9j7UW86TybdQFe.1Ztwi3G1C', 'Kukun Kurniawan', 'User', '2024-03-04 10:08:35.000000', '2024-03-04 10:08:35.000000'),(8, 'user8', '$2b$10$9PQ4l3CIhGck3nWCRmEYieQwNCzVw9j7UW86TybdQFe.1Ztwi3G1C', 'John Doe', 'Admin', '2024-03-04 10:08:35.000000', '2024-03-04 10:08:35.000000'),(9, 'user9', '$2b$10$9PQ4l3CIhGck3nWCRmEYieQwNCzVw9j7UW86TybdQFe.1Ztwi3G1C', 'John Doe', 'User', '2024-03-04 10:08:35.000000', '2024-03-04 10:08:35.000000'),(10, 'user10', '$2b$10$9PQ4l3CIhGck3nWCRmEYieQwNCzVw9j7UW86TybdQFe.1Ztwi3G1C', 'Jane Smith', 'Admin', '2024-03-04 10:08:35.000000', '2024-03-04 10:08:35.000000'),(11, 'hilman', '$2y$12$7F.jmoy94Z9ZU0zo5iP3cuj/A1klR3PjcYdeggB7YTU3ttuR4.Gm6', 'M Hilman', 'User', '2024-07-01 01:40:47.000000', '2024-07-01 01:40:47.000000')
;
UNLOCK TABLES;
COMMIT;
