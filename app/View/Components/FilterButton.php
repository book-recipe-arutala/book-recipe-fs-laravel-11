<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class FilterButton extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(
        public $levelId,
        public $levelsData,
        public $categoryId,
        public $categoriesData,
        public $timeCook,
        public $sortBy,
    ) {
        //
    }

    public function isDefaultSelected(){
        return $this->levelId == '' ? 'selected' : '';
    }

    public function isSelected($id, $valueId){
        return $id == $valueId ? 'selected' : '';
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.filter-button');
    }
}
