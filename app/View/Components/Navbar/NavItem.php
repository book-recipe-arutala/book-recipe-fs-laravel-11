<?php

namespace App\View\Components\Navbar;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Route;
use Illuminate\View\Component;

class NavItem extends Component
{
    /**
     * Create a new component instance.
     */
    // Cara pertama
    // public function __construct(public $route, public $text)
    // {
    //     //
    // }

    // Cara Kedua
    public $route;
    public $text;

    public function __construct($route, $text)
    {
        $this->route = $route;
        $this->text = $text;
    }

    public function isActive()
    {
        return Route::is($this->route) ? 'navbar-active' : '';
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.navbar.nav-item');
    }
}
