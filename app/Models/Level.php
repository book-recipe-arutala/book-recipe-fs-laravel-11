<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    use HasFactory;

    protected $table = 'levels';

    protected $primaryKey = 'level_id';

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modified_time';

    protected $fillable = [
        'level_name',
    ];

    public function recipes()
    {
        return $this->hasMany(Recipe::class, 'level_id', 'level_id');
    }
}
