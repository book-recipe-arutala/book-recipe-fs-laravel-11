<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $primaryKey = 'category_id';

    protected $table = 'categories';

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modified_time';

    protected $fillable = [
        'category_name',
    ];

    public function recipe()
    {
        return $this->hasMany(Recipe::class,'category_id', 'category_id');
    }
}
