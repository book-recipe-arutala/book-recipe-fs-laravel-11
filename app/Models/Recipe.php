<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Recipe extends Model
{
    use HasFactory;

    protected $table = 'recipes';
    protected $primaryKey = 'recipe_id';

    public $incrementing = true;

    const CREATED_AT = 'created_time';
    const UPDATED_AT = 'modified_time';

    protected $fillable = [
        'recipe_id',
        'category_id',
        'level_id',
        'user_id',
        'recipe_name',
        'image_filename',
        'time_cook',
        'ingredient',
        'how_to_cook',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'category_id');
    }

    public function level()
    {
        return $this->belongsTo(Level::class, 'level_id', 'level_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }


    // Local Scope
    public function scopeUserRecipe(Builder $query): void
    {
        $query->where('user_id', Auth::id());
    }

    public function scopeRecipeName(Builder $query): void
    {
        $query->whereRaw('LOWER(recipe_name) LIKE ?', '%' . strtolower(request('recipeName')) . '%');
    }


    public function scopeFilterLevel(Builder $query): void
    {
        if (request('levelId')) {
            $query->where('level_id', request('levelId'));
        }
    }

    public function scopeFilterCategory(Builder $query): void
    {
        if (request('categoryId')) {
            $query->where('category_id', request('categoryId'));
        }
    }

    public function scopeTimeCook(Builder $query): void
    {
        if (request('timeCook') == '0-30') {
            $query->where('time_cook', '<=', 30);
        } elseif (request('timeCook') == '30-60') {
            $query->whereBetween('time_cook', [31, 60]);
        } elseif (request('timeCook') == '60') {
            $query->where('time_cook', '>', 60);
        }
    }

    public function scopeFilterSortBy(Builder $query): void
    {
        if (request('sortBy')) {
            $sortByParts = explode(',', request('sortBy'));
            list($sortBy, $direction) = $sortByParts;
            $direction = strtolower($direction);

            if ($sortBy === 'recipeName') {
                $query->orderBy('recipe_name', $direction);
            } elseif ($sortBy === 'timeCook') {
                $query->orderBy('time_cook', $direction);
            }
        } else {
            $query->orderBy('recipe_name', 'asc');
        }
    }
}
