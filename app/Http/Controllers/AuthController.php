<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function showRegisterForm()
    {
        return view('auth.register');
    }

    public function register(Request $request)
    {
        $request->validate(
            [
                'username' => 'required|string|min:1|max:100|regex:/^[^\s]+$/',
                'fullname' => 'required|string|min:1|max:255',
                'password' => 'required|string|min:6|max:50',
                'confirmPassword' => 'required|string|min:6|max:50|same:password',
            ],
            [
                'username.regex' => 'Format username belum sesuai.',
                'username.required' => 'Kolom username tidak boleh kosong.',
                'username.min' => 'Format username belum sesuai.',
                'username.max' => 'Format username belum sesuai.',

                'fullname.required' => 'Kolom fullname tidak boleh kosong.',
                'fullname.min' => 'Format fullname belum sesuai.',
                'fullname.max' => 'Format fullname belum sesuai.',

                'password.required' => 'Kolom password tidak boleh kosong.',
                'password.min' => 'Format password belum sesuai.',
                'password.max' => 'Format password belum sesuai.',

                'confirmPassword.required' => 'Kolom konfirmasi kata sandi tidak boleh kosong.',
                'confirmPassword.min' => 'Format konfirmasi kata sandi belum sesuai.',
                'confirmPassword.max' => 'Format konfirmasi kata sandi belum sesuai.',
                'confirmPassword.same' => 'Konfirmasi kata sandi tidak sama dengan kata sandi.',
            ]
        );

        try {
            $credentials = $request->only('username', 'password');

            $user = User::where('username', $credentials['username'])->first();

            if ($user) {
                return redirect()->back()->withErrors(['username' => 'Username sudah digunakan'])->withInput();
            }

            $newUser = [
                'username' => $request->input('username'),
                'fullname' => $request->input('fullname'),
                'password' =>  Hash::make($request->input('password')),
                'role' => 'User',
            ];

            User::create($newUser);

            return redirect()->route('login')->with('success', 'Pendaftaran user berhasil! Silahkan lakukan login!.');
        } catch (\Exception $e) {
            Log::error("Register Process Error: " . $e->getMessage());
            return redirect()->back()->withErrors(['error' => 'Terjadi kesalahan server. Silahkan coba kembali.'])->withInput();
        }
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $request->validate(
            [
                'username' => 'required|string|min:1|max:100|regex:/^[^\s]+$/',
                'password' => 'required|string|min:6|max:50'
            ],
            [
                'username.regex' => 'Format username belum sesuai.',
                'username.required' => 'Kolom username tidak boleh kosong.',
                'username.min' => 'Format username belum sesuai.',
                'username.max' => 'Format username belum sesuai.',

                'password.required' => 'Kolom password tidak boleh kosong.',
                'password.min' => 'Format password belum sesuai.',
                'password.max' => 'Format password belum sesuai.'
            ]
        );

        try {
            $credentials = $request->only('username', 'password');

            // if (!$user) {
            //     return redirect()->back()->withErrors(['authError' => 'User tidak ditemukan'], 'auth')->withInput();
            // }

            // if (!Hash::check($credentials['password'], $user->password)) {
            //     return redirect()->back()->withErrors(['authError' => 'Password salah'], 'auth')->withInput();
            // }

            if (!$token = Auth::attempt($credentials)) {
                return redirect()->back()->withErrors(['authError' => 'Username atau password salah!'])->withInput();
            }

            // Jika menggunakan mekanisme token custom seperti JWT, kode ini perlu ditambahkan
            // $user = User::where('username', $credentials['username'])->first();

            // $request->session()->put('token', $token);
            // $request->session()->put('id', $user->userId);

            return redirect()->route('recipe');
        } catch (\Exception $e) {
            Log::error("Error adding Sign in: " . $e->getMessage());
            return redirect()->back()->withErrors(['error' => 'Terjadi kesalahan server. Silahkan coba kembali. '])->withInput();
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/login');
    }
}
