<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Http\Resources\RecipeDetailResource;
use App\Http\Resources\RecipeResource;
use App\Models\Category;
use App\Models\Level;
use App\Models\Recipe;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Throwable;

class RecipeController extends Controller
{
    public function showRecipeList()
    {
        return view('contoh.home-page');
    }

    public function showUserPage()
    {
        $user = Auth::user();
        $username = $user->username;
        $fullname = $user->fullname;
        return view('contoh.user-page', compact('username', 'fullname'));
    }

    public function showMyRecipeUserPage()
    {
        $user = Auth::user();
        $username = $user->username;
        $fullname = $user->fullname;
        return view('contoh.myrecipe-user-page', compact('username', 'fullname'));
    }

    public function showRecipes(Request $request)
    {
        try {
            $recipeName = $request->recipeName;
            $levelId = $request->levelId;;
            $categoryId = $request->categoryId;
            $timeCook = $request->timeCook;
            $sortBy = $request->sortBy;

            $pageSize = $request->input('pageSize', 8);
            $recipes = Recipe::recipeName()->filterLevel()->filterCategory()->timeCook()->filterSortBy()->paginate($pageSize);

            $levelsData = Level::get();
            $categoriesData = Category::get();

            $action = '/recipe';

            return view('page.daftar-resep', compact(
                'recipes',
                'recipeName',
                'levelsData',
                'categoriesData',
                'levelId',
                'categoryId',
                'timeCook',
                'sortBy',
                'pageSize',
                'action'
            ));
        } catch (Exception $e) {
            Log::error("Get daftar resep failed: " . $e->getMessage());
            return redirect()->back()->withErrors(['error' => 'Terjadi kesalahan server. Silahkan coba kembali.']);
        }
    }

    public function showMyRecipes(Request $request)
    {
        try {
            $recipeName = $request->recipeName;
            $levelId = $request->levelId;;
            $categoryId = $request->categoryId;
            $timeCook = $request->timeCook;
            $sortBy = $request->sortBy;

            $pageSize = $request->input('pageSize', 8);
            $recipes = Recipe::recipeName()->userRecipe()->filterLevel()->filterCategory()->timeCook()->filterSortBy()->paginate($pageSize);

            $levelsData = Level::get();
            $categoriesData = Category::get();

            $action = '/recipe/myrecipe';

            return view('page.daftar-resep', compact(
                'recipes',
                'recipeName',
                'levelsData',
                'categoriesData',
                'levelId',
                'categoryId',
                'timeCook',
                'sortBy',
                'pageSize',
                'action'
            ));
        } catch (Exception $e) {
            Log::error("Get daftar resep failed: " . $e->getMessage());
            return redirect()->back()->withErrors(['error' => 'Terjadi kesalahan server. Silahkan coba kembali.']);
        }
    }

    public function exampleRecipe(Request $request)
    {
        try {
            $recipeName = $request->recipeName;
            $levelId = $request->levelId;;
            $categoryId = $request->categoryId;
            $timeCook = $request->timeCook;
            $sortBy = $request->sortBy;
            $pageSize = $request->pageSize;

            $query = Recipe::query();

            if ($levelId) {
                $query->where('level_id', $request->input('levelId'));
            }

            $pageSize = $request->input('pageSize', 8);
            $recipes = $query->paginate($pageSize);

            $levelsData = Level::get();
            $categoriesData = Category::get();

            return view('page.contoh-daftar-resep', compact(
                'recipes',
                'recipeName',
                'levelsData',
                'categoriesData',
                'levelId',
                'categoryId',
                'timeCook',
                'sortBy',
                'pageSize'
            ));
        } catch (Exception $e) {
            Log::error("Get daftar resep failed: " . $e->getMessage());
            return redirect()->back()->withErrors(['error' => 'Terjadi kesalahan server. Silahkan coba kembali.']);
        }
    }

    public function showCreateRecipe()
    {
        $formTittle = "Tambah Resep";

        $levelsData = Level::get();
        $categoriesData = Category::get();

        return view('page.tambah-resep', compact(
            'formTittle',
            'categoriesData',
            'levelsData'
        ));
    }

    // public function show($id)
    // {
    //     try {
    //         $recipe = Recipe::find($id);

    //         if ($recipe === null) {
    //             $resData = responseHelper::response(404, 'Detil Resep masakan tidak tersedia');
    //             return $resData;
    //         }
    //         $recipeResource = new RecipeDetailResource($recipe);

    //         $resData = responseHelper::response(200, 'Berhasil memuat Resep Masakan "' . $recipe->recipe_name . '"', $recipeResource);
    //         return $resData;
    //     } catch (Throwable $error) {
    //         Log::info($error->getMessage());
    //         $resData = responseHelper::response(500, 'Terjadi kesalahan server. Silahkan coba kembali.');
    //         return $resData;
    //     }
    // }

    public function store(Request $request)
    {
        dd($request->input('ingredient'),$request->input('howToCook'));

        // Validasi data request yang masuk
        $request->validate([
            'recipeName' => 'required|string',
            'image' => 'image|mimes:jpg,png,jpeg|max:2048',
            'timeCook' => 'required|integer|min:0|max:999',
            'levelId' => 'required',
            'categoryId' => 'required',
            'howToCook' => 'required|min:12',
            'ingredient' => 'required|min:12',
        ], [
            'ingredient.required' => 'Bahan-bahan tidak boleh kosong',
            'ingredient.min' => 'Bahan-bahan tidak boleh kosong',
            'howToCook.required' => 'Cara memasak tidak boleh kosong',
            'howToCook.min' => 'Cara memasak tidak boleh kosong',

            'recipeName.required' => 'Nama Resep Makanan tidak boleh kosong',
            'recipeName.max' => 'Panjang kolom tidak boleh melebihi 255 karakter',
            'recipeName.regex' => 'Kolom tidak boleh berisi special character/angka',

            'image.required' => 'File harus berupa gambar',
            'image.mimes' => 'File hanya boleh ber-format JPG, PNG dan JPEG',
            'image.max' => 'File maksimal berukuran 2048 KB',

            'timeCook.required' => 'Waktu tidak boleh kosong',
            'timeCook.numeric' => 'Kolom hanya boleh berisi angka 1-999',
            'timeCook.max' => 'Kolom hanya boleh berisi angka 1-999',
            'timeCook.min' => 'Kolom hanya boleh berisi angka 1-999',

            'categoryId.required' => 'Kategori Makanan tidak boleh kosong',
            'levelId.required' => 'Tingkat Kesulitan tidak boleh kosong',
        ]);

        try {
            // Secara default jika lansgun menggunakan store maka name nya akan ter-hash
            // $filePath = $request->file('image')->store('recipe-images', 'public');
         // $filePath = Storage::putFile('recipe-images', $request->file('image'), 'public');

            // Memberikan nama spesifik ke file yang disimpan
            $file = $request->File('image');

            $fileName = $file->hashName(); // nama file di-hash + sudah ada extension
            // Nama file yang spesifik
        //  $originalFileName = $file->getClientOriginalName();
        //  $extension = $file->getExtension();
        //  $fileName = now()->timestamp . '_' . $originalFileName . '_' . $extension;

            $file->storeAs('recipe-images', $fileName, 'public');

            $recipe = Recipe::create([
                'category_id' => $request->input('categoryId'),
                'level_id' => $request->input('levelId'),
                'recipe_name' => $request->input('recipeName'),
                'image_filename' => $fileName,
                'time_cook' => $request->input('timeCook'),
                'ingredient' => $request->input('ingredient'),
                'how_to_cook' => $request->input('howToCook'),
                'user_id' => Auth::user()->user_id,
            ]);

            return redirect()->route('recipe')->with('success', 'Recipe ' . $recipe->level_name . ' successfully created.');
        } catch (\Exception $e) {
            Log::error("Create Recipe Process Error: " . $e->getMessage());
            return redirect()->back()->withErrors(['error' => 'Terjadi kesalahan server. Silahkan coba kembali.'])->withInput();
        }
    }

    public function showEditRecipe($id)
    {
        $formTittle = "Edit Resep";

        $data = Recipe::find($id);

        $levelsData = Level::get();
        $categoriesData = Category::get();

        return view('page.edit-resep', compact(
            'formTittle',
            'categoriesData',
            'levelsData',
            'data',
        ));
    }

    public function update(Request $request, $recipeid)
    {
        dd($request->input('ingredient'), $request->input('howToCook'));
        try {
            $request->validate([
                'recipeName' => 'required|string',
                'image' => 'image|mimes:jpg,png,jpeg|max:2048',
                'timeCook' => 'required|integer|min:0|max:999',
                'levelId' => 'required',
                'categoryId' => 'required',
                'howToCook' => 'required|min:12',
                'ingredient' => 'required|min:12',
            ], [
                'ingredient.required' => 'Bahan - Bahan tidak boleh kosong',
                'ingredient.min' => 'Bahan - Bahan tidak boleh kosong',
                'howToCook.required' => 'Bahan - Bahan tidak boleh kosong',
                'howToCook.min' => 'Bahan - Bahan tidak boleh kosong',

                'recipeName.required' => 'Nama Resep Makanan tidak boleh kosong',
                'recipeName.max' => 'Panjang kolom tidak boleh melebihi 255 karakter',
                'recipeName.regex' => 'Kolom tidak boleh berisi special character/angka',

                'image.required' => 'File harus berupa gambar',
                'image.mimes' => 'File hanya boleh ber-format JPG, PNG dan JPEG',
                'image.max' => 'File maksimal berukuran 2048 KB',

                'timeCook.required' => 'Waktu tidak boleh kosong',
                'timeCook.numeric' => 'Kolom hanya boleh berisi angka 1-999',
                'timeCook.max' => 'Kolom hanya boleh berisi angka 1-999',
                'timeCook.min' => 'Kolom hanya boleh berisi angka 1-999',

                'categoryId.required' => 'Kategori Makanan tidak boleh kosong',
                'levelId.required' => 'Tingkat Kesulitan tidak boleh kosong',
            ]);

            $recipe = Recipe::find($recipeid);

            $categoryId = $request->input('categoryId');
            $levelId = $request->input('levelId');
            $recipe->category_id = $categoryId;
            $recipe->level_id = $levelId;
            $recipe->recipe_name = $request->input('recipeName');
            $recipe->time_cook = $request->input('timeCook');
            $recipe->ingredient = $request->input('ingredient');
            $recipe->how_to_cook = $request->input('howToCook');

            if ($request->hasFile('image')) {
                if ($recipe->image_filename) {
                    Storage::disk('public')->delete('recipe-images/' . $recipe->image_filename);
                }
                $file = $request->File('image');
                $fileName = $file->hashName();

                $file->storeAs('recipe-images', $fileName, 'public');

                $recipe->image_filename = $fileName;
            }

            $recipe->save();

            return redirect()->route('recipe')->with('success', 'Recipe ' . $recipe->level_name . ' successfully updated.');
        } catch (\Exception $e) {
            Log::error("Update Recipe Process Error: " . $e->getMessage());
            return redirect()->back()->withErrors(['error' => 'Terjadi kesalahan server. Silahkan coba kembali.'])->withInput();
        }
    }

    // public function destroy($id)
    // {
    //     try {
    //         $recipe = Recipe::find($id);

    //         if (!$recipe) {
    //             $resData = responseHelper::response(404, 'Resep tidak ditemukan');
    //             return response()->json($resData, 404);
    //         }

    //         $recipe->delete();

    //         $resData = ResponseHelper::response(200, 'Resep "' . $recipe->recipe_name . '" berhasil dihapus!', "", 0);

    //         return $resData;
    //     } catch (Throwable $error) {
    //         Log::info($error->getMessage());
    //         $resData = responseHelper::response(500, 'Terjadi kesalahan server. Silahkan coba kembali');
    //         return response()->json($resData, 500);
    //     }
    // }
}
