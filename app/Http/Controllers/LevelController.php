<?php

namespace App\Http\Controllers;

use App\Http\Resources\LevelResource;
use App\Models\Level;
use Illuminate\Http\Request;

class LevelController extends Controller
{
    public function showLevels()
    {
        $levels = Level::all();

        return view('contoh-crud.level-page', compact('levels'));
    }

    public function showLevelDetail($id)
    {
        $level = Level::find($id);

        return view('contoh-crud.level-page-detail', compact('level'));
    }

    public function createLevel()
    {
        return view('contoh-crud.level-create');
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                'levelName' => 'required|string|max:255',
            ],
            [
                'levelName.required' => 'Harus ada input untuk nama level.',
                'levelName.string' => 'Nama level harus berupa teks.',
                'levelName.max' => 'Nama level maksimal 255 karakter.',
            ]
        );

        $level = Level::create([
            'level_name' => $request->input('levelName')
        ]);

        return redirect()->route('levels.index')->with('success', 'Level ' . $level->level_name . ' successfully created.');
    }

    public function editLevel($id)
    {
        $level = Level::findOrFail($id);
        return view('contoh-crud.level-edit', compact('level'));
    }

    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'levelName' => 'required|string|max:255',
            ],
            [
                'levelName.required' => 'Harus ada input untuk nama level.',
                'levelName.string' => 'Nama level harus berupa teks.',
                'levelName.max' => 'Nama level maksimal 255 karakter.',
            ]
        );

        $level = Level::find($id);

        if (is_null($level)) {
            return redirect()->route('levels.index')->with('error', 'Level not found.');
        }

        $level->level_name = $request->input('levelName');
        $level->save();

        return redirect()->route('levels.index')->with('success', 'Level updated successfully.');
    }

    public function destroy($id)
    {
        $level = Level::find($id);

        $levelName = $level->levelName;

        $level->delete();

        return redirect()->route('levels.index')->with('success', 'Level ' . $levelName . ' delete successfully.');
    }
}
