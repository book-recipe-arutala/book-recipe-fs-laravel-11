<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\LevelController;
use App\Http\Controllers\RecipeController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/contoh', function () {
    return view('contoh.contoh-page');
});

Route::get('/contoh/home', function () {
    return view('contoh.home-page');
})->name('contoh.home');

Route::get('/contoh/list', function () {
    return view('contoh.list-page');
})->name('contoh.list');

// Level CRUD
Route::get('/level', [LevelController::class, 'showLevels'])->name('levels.index');
Route::get('/level/create', [LevelController::class, 'createLevel'])->name('levels.create');
Route::get('/level/{id}', [LevelController::class, 'showLevelDetail'])->name('levels.detail');
Route::get('/level/{id}/update', [LevelController::class, 'editLevel'])->name('level.edit');

Route::post('/level', [LevelController::class, 'store']);
Route::put('/level/{id}', [LevelController::class, 'update'])->name('levels.update');
Route::delete('/level/{id}', [LevelController::class, 'destroy'])->name('levels.destroy');
Route::delete('/level/{id}/delete', [LevelController::class, 'destroy']);

//

// Auth Form
// Jika ada URL yang sama tetapi HTTP Request berbeda, di route bisa diwakilkan oleh satu name saja
Route::get('/register', [AuthController::class, 'showRegisterForm'])->name('register');
Route::post('/register', [AuthController::class, 'register']);

Route::middleware(['guest'])->group(function () {
    Route::get('/login', [AuthController::class, 'showLoginForm'])->name('login');
Route::post('/login', [AuthController::class, 'login']);
});

Route::middleware(['auth'])->group(function () {
    // Route::get('/recipe', [RecipeController::class, 'showUserPage'])->name('recipe');
    // Route::get('/recipe/myrecipe', [RecipeController::class, 'showMyRecipeUserPage'])->name('recipe.myrecipes');

    Route::get('/example/recipe', [RecipeController::class, 'exampleRecipe']);

    Route::get('/recipe', [RecipeController::class, 'showRecipes'])->name('recipe');
    Route::get('/recipe/myrecipe', [RecipeController::class, 'showMyRecipes'])->name('recipe.myrecipes');
    Route::get('/recipe/create', [RecipeController::class, 'showCreateRecipe'])->name('recipe.create');
    Route::get('/recipe/{id}', [RecipeController::class, 'showRecipeList'])->name('recipe.detail');
    Route::get('/recipe/{id}/edit', [RecipeController::class, 'showEditRecipe'])->name('recipe.edit');

    Route::post('/recipe', [RecipeController::class, 'store']);
    Route::put('/recipe/{id}/edit', [RecipeController::class, 'update']);
});

Route::post('/logout', [AuthController::class, 'logout'])->name('logout');
