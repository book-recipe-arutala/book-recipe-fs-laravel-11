@extends('layouts.master')
@section('title', 'Daftar Resep')

@section('content')
    <h2 class="text-center fw-bolder mb-5 mt-3">{{ $formTittle }}</h2>
    <form action="{{ route('recipe') }}" class="mb-5" method="POST" enctype="multipart/form-data" id="recipe-form">
        @csrf
        <div class="row">
            <div class="col-md-6">
                <div>
                    <label for="recipeName">Nama Resep Masakan <strong class="text-danger ">*</strong></label>
                    <input id="recipeName" class="form-control mt-2 @error('recipeName') border-danger @enderror"
                        type="text" name="recipeName" placeholder="Nama Resep Masakan">
                    @error('recipeName')
                        <p class="text-danger m-0 p-0">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mt-4">
                    <label for="image">Gambar Masakan <strong class="text-danger ">*</strong></label>
                    {{-- <input id="image" class="form-control mt-2 @error('image') border-danger @enderror" type="file"
                        name="image" placeholder="Silahkan Masukkan Gambar"> --}}
                    <input id="image" class="form-control mt-2 @error('image') border-danger @enderror" type="file"
                        name="image" placeholder="Silahkan Masukkan Gambar" style="border-radius: 6px 6px 0px 0px">
                    <div class="form-control" style="height: 160px; border-radius: 0px 0px 6px 6px">
                        {{-- Gambar default --}}
                        <img id="default-image" src="{{ Storage::url('recipe-images/default.png') }}" alt="Default Image"
                            class="img-fluid" style="height: 100%; width: 100%; object-fit: contain;">
                        {{-- Gambar jika sudah upload --}}
                        <img id="preview-image" src="#" alt="Preview Image" class="img-fluid"
                            style="height: 100%; width: 100%; object-fit: contain; display: none">
                    </div>
                    @error('image')
                        <p class="text-danger m-0 p-0">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mt-4">
                    <input id="image" class="form-control mt-2 @error('image') border-danger @enderror" type="file"
                        name="image" placeholder="Silahkan Masukkan Gambar">
                    <input id="image" type="file" name="image" placeholder="Silahkan Masukkan Gambar"
                        class="file-input-custom">
                    <label>Bahan-Bahan <strong class="text-danger ">*</strong></label>
                    @error('image')
                        <p class="text-danger m-0 p-0">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mt-4">
                    <label>Bahan-Bahan <strong class="text-danger ">*</strong></label>
                    <div class="@error('ingredient') rich-text-danger @enderror">
                        <div id="ingredientEditor"></div>
                        <textarea name="ingredient" id="ingredientTextArea" style="display: none"></textarea>
                    </div>
                    @error('ingredient')
                        <p class="text-danger m-0 p-0">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="col-md-6">
                <div>
                    <label for="category">Kategori Masakan <strong class="text-danger ">*</strong></label>
                    <select id="category" name="categoryId"
                        class="form-select mt-2 @error('categoryId') border-danger @enderror" placeholder>
                        <option value='' selected disabled class="text-center">Pilih Kategori</option>
                        @foreach ($categoriesData as $category)
                            <option value="{{ $category->category_id }}">
                                {{ $category->category_name }}
                            </option>
                        @endforeach
                    </select>
                    @error('selectedCategory')
                        <p class="text-danger m-0 p-0">{{ $message }}</p>
                    @enderror
                </div>
                <div class="row mt-4">
                    <div class="col-md-6">
                        <label for="time">Waktu Memasak (Menit) <strong class="text-danger ">*</strong></label>
                        <input id="time" class="form-control mt-2 @error('timeCook') border-danger @enderror"
                            type="number" name="timeCook" placeholder="Waktu Memasak">
                        @error('timeCook')
                            <p class="text-danger m-0 p-0">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <label for="level">
                            Tingkat Kesulitan <strong class="text-danger ">*</strong>
                        </label>
                        <select id="level" name="levelId"
                            class="form-select mt-2 @error('levelId') border-danger @enderror">
                            <option value='' selected disabled>Pilih Tingkat Kesulitan</option>
                            @foreach ($levelsData as $level)
                                <option value="{{ $level->level_id }}">{{ $level->level_name }}</option>
                            @endforeach
                        </select>
                        @error('selectedLevel')
                            <p class="text-danger m-0 p-0">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                <div class="mt-4">
                    <label for="howToCookEditor">Cara Masak <strong class="text-danger ">*</strong></label>
                    <div class="@error('howToCook') rich-text-danger @enderror">
                        <div id="howToCookEditor"></div>
                        <textarea name="howToCook" id="howToCookTextArea" style="display: none"></textarea>
                    </div>
                    @error('howToCook')
                        <p class="text-danger m-0 p-0">{{ $message }}</p>
                    @enderror
                </div>
                <div class="d-flex justify-content-end my-4 buttons-container">
                    <button class="btn btn-recipe-outline-primary mb-md-0 me-2" wire:click='cancel'>Batal</button>
                    <button type="submit" class="btn btn-recipe-primary">Submit</button>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('script')
    <script>
        document.getElementById('image').addEventListener('change', function(event) {
            var reader = new FileReader();
            reader.onload = function() {
                var previewImage = document.getElementById('preview-image');
                previewImage.src = reader.result;
                previewImage.style.display = 'block';

                var defaultImage = document.getElementById('default-image');
                defaultImage.style.display = 'none';
            };
            reader.readAsDataURL(event.target.files[0]);
        });

        const ingredientEditor = new Quill('#ingredientEditor', {
            theme: 'snow'
        });
        const howToCookEditor = new Quill('#howToCookEditor', {
            theme: 'snow'
        });

        $('#recipe-form').on('submit', function() {
            // Nilai yang dikirim ke controller akan null jika kondisi tidak terpenuhi
            // if (ingredientEditor.root.innerHTML !== '<p><br></p>' && ingredientEditor.root.innerHTML.trim() !== '') {
            //     $('#ingredientTextArea').val(ingredientEditor.root.innerHTML);
            // }

            $('#ingredientTextArea').val(ingredientEditor.root.innerHTML);
            $('#howToCookTextArea').val(ingredientEditor.root.innerHTML);
        });
    </script>
@endsection
