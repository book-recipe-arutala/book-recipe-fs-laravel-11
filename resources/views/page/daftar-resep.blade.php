@extends('layouts.master')
@section('title', 'Daftar Resep')

@section('content')
    <div class="col-12 mb-5">

        <form action="{{$action}}" method="GET">
            <div class="d-none d-sm-flex flex-column align-items-center mt-5">
                <div class="d-flex justify-content-center col-12">
                    <a href="{{ route('recipe.create') }}" class="btn-add btn btn-primary mx-3 align-items-center"
                        role="button">
                        <i class="bi bi-plus-lg"></i> Tambah Resep
                    </a>
                    <x-search-bar :$recipeName />
                    <x-filter-button :$levelId :$levelsData :$categoryId :$categoriesData :$timeCook :$sortBy />
                    <input type="hidden" name="pageSize" value="{{ $pageSize }}">
                </div>
                <h1 class="fw-semibold mt-4">Daftar Resep</h1>
            </div>

            {{-- <div class="d-flex flex-column justify-content-center mt-3 mx-2 d-sm-none">
            <h1 class="fw-bold custom-fontsize-subtitle text-center">Daftar Resep Masakan</h1>
            <div class="mb-3">
                <livewire:component.search class="custom-fontsize-content1">
            </div>
            <div class="d-flex flex-column justify-content-center filter-add gap-3 w-100">
                <livewire:component.filter :isMobile="true" class="custom-fontsize-content1">
                <a href="{{ route('recipe.create') }}" class="btn-add btn btn-primary w-100" role="button">
                    <i class="fa-solid fa-plus custom-fontsize-content1"></i> Tambah Resep
                </a>
            </div>
        </div> --}}
        </form>

        <div class="justify-content-center align-items-center w-100">
            <div class="row justify-content-start mt-3">

                @forelse ($recipes as $data)
                    <x-card :$data />
                @empty
                    <div>
                        <h1>Data resep tidak ada!</h1>
                    </div>
                @endforelse
                <div class="mx-md-2">
                    <div class="d-md-flex align-items-center justify-content-between mt-4 mx-auto">
                        <form action="{{$action}}" method="GET">
                            <input type="hidden" name="recipeName" value="{{ $recipeName }}">
                            <input type="hidden" name="levelId" value="{{ $levelId }}">
                            <input type="hidden" name="categoryId" value="{{ $categoryId }}">
                            <input type="hidden" name="sortBy" value="{{ $sortBy }}">
                            <input type="hidden" name="timeCook" value="{{ $timeCook }}">
                            <x-entries :selectedEntries="$pageSize" />
                        </form>
                        {{$recipes->links('pagination::custom-pagination')}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
