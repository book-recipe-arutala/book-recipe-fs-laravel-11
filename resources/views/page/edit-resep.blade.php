@extends('layouts.master')
@section('title', 'Edit Resep')

@section('content')
    <h2 class="text-center fw-bolder mb-5 mt-3">{{ $formTittle }}</h2>
    {{-- <form action="{{ route('recipe.edit', ['id' => $data->recipe_id]) }}" class="mb-5" method="POST"
        enctype="multipart/form-data" id="recipe-form"> --}}
    <form action="{{ route('recipe.edit', ['id' => $data->recipe_id]) }}" class="mb-5"
        method="POST"enctype="multipart/form-data" id="recipe-form">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col-md-6">
                <div>
                    <label for="recipeName">Nama Resep Masakan <strong class="text-danger ">*</strong></label>
                    <input id="recipeName" class="form-control mt-2 @error('recipeName') border-danger @enderror"
                        type="text" name="recipeName" placeholder="Nama Resep Masakan" value="{{ $data->recipe_name }}">
                    @error('recipeName')
                        <p class="text-danger m-0 p-0">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mt-4">
                    <label for="img">Gambar Masakan <strong class="text-danger ">*</strong></label>
                    <input id="image" class="form-control mt-2 @error('image') border-danger @enderror" type="file"
                        name="image" placeholder="Silahkan Masukkan Gambar" style="border-radius: 6px 6px 0px 0px">
                    <div class="form-control" style="height: 160px; border-radius: 0px 0px 6px 6px">
                        @if (Str::isUrl($data->image_filename))
                            <img src="{{ $data->image_filename }}" alt="{{ $data->recipe_name }}" class="img-fluid"
                                style="height: 100%; width: 100%; object-fit: contain;">
                        @elseif ($data->image_filename != null && Storage::disk('public')->exists('recipe-images/' . $data->image_filename))
                            <img src="{{ Storage::url('recipe-images/' . $data->image_filename) }}"
                                alt="{{ $data->recipe_name }}" class="img-fluid"
                                style="height: 100%; width: 100%; object-fit: contain;">
                        @else
                            <img src="{{ Storage::url('recipe-images/default.png') }}" alt="{{ $data->recipe_name }}"
                                class="img-fluid" style="height: 100%; width: 100%; object-fit: contain;">
                        @endif
                    </div>
                    @error('image')
                        <p class="text-danger m-0 p-0">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mt-4">
                    <label>Bahan-Bahan <strong class="text-danger ">*</strong></label>
                    <div class="@error('ingredient') rich-text-danger @enderror">
                        <div id="ingredientEditor"></div>
                        <textarea name="ingredient" id="ingredientTextArea" style="display:none;">{!! $data->ingredient !!}</textarea>
                    </div>
                    @error('ingredient')
                        <p class="text-danger m-0 p-0">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="col-md-6">
                <div>
                    <label for="category">Kategori Masakan <strong class="text-danger ">*</strong></label>
                    <select id="category" name="categoryId"
                        class="form-select mt-2 @error('categoryId') border-danger @enderror" placeholder>
                        <option value='' {{ $data->category_id == '' ? 'selected' : '' }} disabled
                            class="text-center">Pilih Kategori</option>
                        @foreach ($categoriesData as $category)
                            <option value="{{ $category->category_id }}"
                                {{ $category->category_id == '' ? 'selected' : '' }}>
                                {{ $category->category_name }}
                            </option>
                        @endforeach
                    </select>
                    @error('selectedCategory')
                        <p class="text-danger m-0 p-0">{{ $message }}</p>
                    @enderror
                </div>
                <div class="row mt-4">
                    <div class="col-md-6">
                        <label for="time">Waktu Memasak (Menit) <strong class="text-danger ">*</strong></label>
                        <input id="time" class="form-control mt-2 @error('timeCook') border-danger @enderror"
                            type="number" name="timeCook" placeholder="Waktu Memasak" value={{ $data->time_cook }}>
                        @error('timeCook')
                            <p class="text-danger m-0 p-0">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <label for="level">
                            Tingkat Kesulitan <strong class="text-danger ">*</strong>
                        </label>
                        <select id="level" name="levelId"
                            class="form-select mt-2 @error('levelId') border-danger @enderror">
                            <option value='' disabled {{ $data->level_id == '' ? 'selected' : '' }}>Pilih Tingkat
                                Kesulitan</option>
                            @foreach ($levelsData as $level)
                                <option value="{{ $level->level_id }}"
                                    {{ $data->level_id == $level->level_id ? 'selected' : '' }}>
                                    {{ $level->level_name }}
                                </option>
                            @endforeach
                        </select>
                        @error('selectedLevel')
                            <p class="text-danger m-0 p-0">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                <div class="mt-4">
                    <label for="howToCookEditor">Cara Masak <strong class="text-danger ">*</strong></label>
                    <div class="@error('howToCook') rich-text-danger @enderror">
                        <div id="howToCookEditor"></div>
                        <textarea name="howToCook" id="howToCookTextArea" style="display:none;">{!! $data->how_to_cook !!}</textarea>
                    </div>
                    @error('howToCook')
                        <p class="text-danger m-0 p-0">{{ $message }}</p>
                    @enderror
                </div>
                <div class="d-flex justify-content-end my-4 buttons-container">
                    <button class="btn btn-recipe-outline-primary mb-md-0 me-2" wire:click='cancel'>Batal</button>
                    <button type="submit" class="btn btn-recipe-primary">Submit</button>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('script')
    <script>
        const ingredientEditor = new Quill('#ingredientEditor', {
            theme: 'snow'
        });

        const howToCookEditor = new Quill('#howToCookEditor', {
            theme: 'snow'
        });

        // Isi dari editor diambil dari value yang disimpan di Text Area Ingredent
        // Identifier text editor berdasarkan id pada textarea
        ingredientEditor.root.innerHTML = $('#ingredientTextArea').val();
        howToCookEditor.root.innerHTML = $('#howToCookTextArea').val();

        $('#recipe-form').on('submit', function() {
            $('#ingredientTextArea').val(ingredientEditor.root.innerHTML);
            $('#howToCookTextArea').val(howToCookEditor.root.innerHTML);
        });
    </script>
@endsection
