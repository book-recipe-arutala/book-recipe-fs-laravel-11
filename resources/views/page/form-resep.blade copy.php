@extends('layouts.master')
@section('title', 'Daftar Resep')

@section('content')
    <style>
        .drag-area {
            border: 2px dashed #D9D9D9;
            padding: 20px;
            text-align: center;
            cursor: pointer;
            transition: background-color 0.3s;
        }

        .drag-area.dragover {
            background-color: #f0f0f0;
        }

        .preview {
            margin-top: 20px;
            text-align: center;
        }

        .preview img {
            max-width: 100%;
            height: auto;
        }
    </style>
    <h2 class="text-center fw-bolder mb-5 mt-3">{{ $formTittle }}</h2>
    <form action="">
        <div class="row">
            <div class="col-md-6">
                <div>
                    <label for="recipeName">Nama Resep Masakan <strong class="text-danger ">*</strong></label>
                    <input id="recipeName" class="form-control mt-2 @error('recipeName') border-danger @enderror"
                        type="text" name="recipeName" placeholder="Nama Resep Masakan">
                    @error('recipeName')
                        <p class="text-danger m-0 p-0">{{ $message }}</p>
                    @enderror
                </div>
                <div>
                    <label for="image">Gambar Masakan <strong class="text-danger ">*</strong></label>
                    {{-- <input id="image" class="form-control mt-2 @error('recipeName') border-danger @enderror"
                        type="file" name="image" placeholder="Silahkan Masukkan Gambar"> --}}
                    {{-- <div id="image"
                        class="form-control mt-2 @error('recipeName') border-danger @enderror
                    d-flex align-items-center justify-content-center" style="cursor: pointer">
                        <div style="max-width: 260px; text-align: center;">
                            <i class="bi bi-image" style="color: #D9D9D9; font-size: 50px"></i>
                            <p style="color: #787885">
                                <strong>Click to upload</strong> or drag and drop PNG, JPG, JPEG (MAX 2 MB)
                            </p>
                        </div>
                    </div>
                    <input type="file" name="recipeImage" id="image-input" class="d-none"
                        accept="image/png, image/jpeg, image/jpg">
                    <script>
                        document.getElementById('image').addEventListener('click', function() {
                            document.getElementById('image-input').click();
                        });
                    </script> --}}
                    <div id="image"
                        class="form-control mt-2 @error('recipeName') border-danger @enderror
        d-flex align-items-center justify-content-center drag-area">
                        <div id="upload-instructions" style="max-width: 260px; text-align: center;">
                            <i class="bi bi-image" style="color: #D9D9D9; font-size: 50px"></i>
                            <p style="color: #787885">
                                <strong>Click to upload</strong> or drag and drop PNG, JPG, JPEG (MAX 2 MB)
                            </p>
                        </div>
                        <div class="preview" id="preview"></div>
                    </div>
                    <input type="file" name="recipeImage" id="image-input" class="d-none"
                        accept="image/png, image/jpeg, image/jpg">

                    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
                    <script>
                        $(document).ready(function() {
                            var dropArea = $('#image');
                            var input = $('#image-input');
                            var preview = $('#preview');
                            var instructions = $('#upload-instructions');

                            // Prevent default behavior for drag events
                            dropArea.on('dragenter dragover dragleave drop', function(e) {
                                e.preventDefault();
                                e.stopPropagation();
                            });

                            // Highlight drop area when file is dragged over it
                            dropArea.on('dragenter dragover', function() {
                                dropArea.addClass('dragover');
                            });

                            dropArea.on('dragleave drop', function() {
                                dropArea.removeClass('dragover');
                            });

                            // Handle drop event
                            dropArea.on('drop', function(e) {
                                var files = e.originalEvent.dataTransfer.files;
                                input[0].files = files;
                                handleFiles(files);
                            });

                            // Trigger input file click on area click
                            dropArea.on('click', function() {
                                input.click();
                            });

                            // Handle input file change event
                            input.on('change', function(e) {
                                handleFiles(e.target.files);
                            });

                            function handleFiles(files) {
                                preview.empty();
                                if (files.length > 0) {
                                    instructions.hide();
                                } else {
                                    instructions.show();
                                }
                                for (var i = 0; i < files.length; i++) {
                                    var file = files[i];
                                    if (file.type.startsWith('image/')) {
                                        var reader = new FileReader();
                                        reader.onload = (function(file) {
                                            return function(e) {
                                                var img = $('<img>').attr('src', e.target.result);
                                                preview.append(img);
                                            };
                                        })(file);
                                        reader.readAsDataURL(file);
                                    }
                                }
                            }
                        });
                    </script>
                    @error('recipeName')
                        <p class="text-danger m-0 p-0">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="col-md-6">
                <div>
                    <label for="category">Kategori Masakan <strong class="text-danger ">*</strong></label>
                    <select id="category" wire:model="selectedCategory"
                        class="form-select mt-2 @error('selectedCategory') border-danger @enderror" placeholder>
                        <option value=null selected disabled class="text-center">Pilih Kategori</option>
                        @foreach ($categoriesData as $category)
                            <option value="{{ $category['categoryId'] }}">
                                {{ $category['categoryName'] }}
                            </option>
                        @endforeach
                    </select>
                    @error('selectedCategory')
                        <p class="text-danger m-0 p-0">{{ $message }}</p>
                    @enderror
                </div>
                <div class="row mt-4">
                    <div class="col-md-6">
                        <label for="time">Waktu Memasak (Menit) <strong class="text-danger ">*</strong></label>
                        <input id="time" class="form-control mt-2 @error('timeCook') border-danger @enderror"
                            type="number" wire:model.live="timeCook" placeholder="Waktu Memasak">
                        @error('timeCook')
                            <p class="text-danger m-0 p-0">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <label for="level">
                            Tingkat Kesulitan <strong class="text-danger ">*</strong>
                        </label>
                        <select id="level" name="selectedLevel"
                            class="form-select mt-2 @error('selectedLevel') border-danger @enderror">
                            <option value='' selected disabled>Pilih Tingkat Kesulitan</option>
                            @foreach ($levelsData as $level)
                                <option value="{{ $level->level_id }}">{{ $level->level_name }}</option>
                            @endforeach
                        </select>
                        @error('selectedLevel')
                            <p class="text-danger m-0 p-0">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
            </div>
        </div>
    </form>
    {{-- <div class="mx-auto p-4">
        <form wire:submit.prevent="submit" class="form-recipe">
            <h2 class="text-center fw-bolder mb-5 mt-3">{{ $formTittle }}</h2>
            <div class="row mx-auto my-2 justify-content-center">
                <div class="col-md-5 mx-2">
                    <div>
                        <label for="recipeName" class="fs-6">Nama Resep Masakan<strong
                                class="text-danger ">*</strong></label>
                        <input id="recipeName" class="form-control mt-2 @error('recipeName') border-danger @enderror"
                            type="text" wire:model.live="recipeName" placeholder="Nama Resep Masakan">
                        @error('recipeName')
                            <p class="text-danger m-0 p-0">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="mt-4">
                        <label for="image" class="fs-6">Gambar Makanan (URL)*<strong
                                class="text-danger ">*</strong></label>
                        <input id="image" class="form-control mt-2 @error('image') border-danger @enderror"
                            type="text" wire:model.live="image" placeholder="Gambar Masakan (URL)">
                        @error('image')
                            <p class="text-danger m-0 p-0">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="mt-4 fs-6">
                        <label>Bahan-bahan<strong class="text-danger ">*</strong></label>
                        <div class="@error('howToCook') rich-text-danger @enderror">
                            <livewire:reusable.rich-text name="ingredient" value="{!! old('ingredient') !!}" />
                        </div>
                        @error('howToCook')
                            <p class="text-danger m-0 p-0">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                <div class="col-md-5 mx-2">
                    <div>
                        <label for="category" class="fs-6">Kategori Masakan<strong class="text-danger ">*</strong></label>
                        <select id="category" wire:model="selectedCategory"
                            class="form-select mt-2 @error('selectedCategory') border-danger @enderror" placeholder>
                            <option value=null selected disabled class="text-center">Pilih Kategori</option>
                            @foreach ($categoriesData as $category)
                                <option value="{{ $category['categoryId'] }}">
                                    {{ $category['categoryName'] }}
                                </option>
                            @endforeach
                        </select>
                        @error('selectedCategory')
                            <p class="text-danger m-0 p-0">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="row mt-4">
                        <div class="col-md-6">
                            <label for="time" class="fs-6">Waktu Memasak (Menit)<strong
                                    class="text-danger ">*</strong></label>
                            <input id="time" class="form-control mt-2 @error('timeCook') border-danger @enderror"
                                type="number" wire:model.live="timeCook" placeholder="Waktu Memasak">
                            @error('timeCook')
                                <p class="text-danger m-0 p-0">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="level" class="fs-6">
                                Tingkat Kesulitan<strong class="text-danger ">*</strong>
                            </label>
                            <select id="level" wire:model="selectedLevel"
                                class="form-select mt-2 @error('selectedLevel') border-danger @enderror">
                                <option value=null selected disabled>Pilih Tingkat Kesulitan</option>
                                @foreach ($levelsData as $level)
                                    <option value="{{ $level['levelId'] }}">{{ $level['levelName'] }}</option>
                                @endforeach
                            </select>
                            @error('selectedLevel')
                                <p class="text-danger m-0 p-0">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="mt-4 fs-6">
                        <label>Cara Memasak<strong class="text-danger ">*</strong></label>
                        <div class="@error('howToCook') rich-text-danger @enderror">
                            <livewire:reusable.rich-text name="howToCook" value="{!! old('howToCook') !!}" />
                        </div>
                        @error('howToCook')
                            <p class="text-danger m-0 p-0">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="d-flex justify-content-end my-3 buttons-container">
                        <button class="btn btn-recipe-outline-primary mb-md-0 me-2" wire:click='cancel'>Batal</button>
                        <button type="submit" class="btn btn-recipe-primary">Submit</button>
                    </div>

                </div>
            </div>
        </form>
    </div> --}}

@endsection
