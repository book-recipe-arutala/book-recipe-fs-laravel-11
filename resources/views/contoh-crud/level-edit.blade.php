@extends('layouts.level-contoh-layout')
@section('title', 'Edit Level')

@section('content')
    <div class="container">
        <button type="button" class="btn btn-default mb-3" style="border-color: black" onclick="window.history.back();">
            <i class="bi bi-arrow-bar-left"></i>
        </button>
        <form class="row g-3 align-items-center" action="/level/{{ $level->level_id }}" method="POST">
            @csrf
            @method('PUT')
            <div class="col-auto ">
                <label for="inputLevelName">Masukkan Nama level</label>
            </div>
            <div class="col-auto">
                <input class="form-control" type="text" placeholder="Nama Level" aria-label="default input example"
                    name="levelName" value="{{ $level->level_name }}" id="inputLevelName">
            </div>
            <div class="col-auto">
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </form>
        @error('levelName')
            <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>
@endsection
