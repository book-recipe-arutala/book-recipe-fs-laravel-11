@extends('layouts.level-contoh-layout')
@section('title', 'List Level')

@section('content')
    <table class="container">
        <tr>
            <th>No</th>
            <th>Level Name</th>
            <th>Detail</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        @foreach ($levels as $level)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $level->level_name }}</td>
                <td><a href="/level/{{ $level->level_id }}"><i class="bi bi-eye-fill btn btn-success"></i></a></td>
                <td><a href="/level/{{ $level->level_id }}/update"><i class="bi bi-pencil btn btn-primary"></i></a></td>
                <td>
                    <div class="hstack d-flex justify-content-center">
                        {{-- Button : konfirmasi tanpa modal --}}
                        <form action="/level/{{ $level->level_id }}" method="POST"
                            onsubmit="return confirm('Ingin hapus level {{ $level->level_name }} ?')">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-warning bi-trash3-fill"></button>
                        </form>
                        &nbsp;
                        {{-- Button : Jika menggunakan modal dalam loop --}}
                        <button type="button" class="btn btn-danger bi-trash3-fill" data-bs-toggle="modal"
                            data-bs-target="#levelDeleteModal-{{ $level->level_id }}">
                        </button>
                    </div>
                </td>
            </tr>
            {{-- Modal --}}
            <div class="modal fade" id="levelDeleteModal-{{ $level->level_id }}" tabindex="-1"
                aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="exampleModalLabel">Delete Level</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <p>
                                Apakah anda yakin ingin <b>menghapus</b> level <b>{{ $level->level_name }}</b> ?
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <form action="{{ route('levels.destroy', $level->level_id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            {{-- End Modal --}}
        @endforeach
    </table>
@endsection
