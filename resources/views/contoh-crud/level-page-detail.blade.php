@extends('layouts.level-contoh-layout')
@section('title', 'Detail Level')

@section('content')
    <a class="btn btn-default mb-3" style="border-color: black" href="{{ route('levels.index') }}">
        <i class="bi bi-arrow-bar-left"></i>
    </a>
    @if ($level == null)
        <h2>Data Level Tidak Ditemukan!</h2>
    @else
        <table class="container">
            <tr>
                <th>Level Id</th>
                <th>Level Name</th>
                <th>Created Date</th>
                <th>Modified Date</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            <tr>
                <td>{{ $level->level_id }}</td>
                <td>{{ $level->level_name }}</td>
                <td>{{ $level->created_time }}</td>
                <td>{{ $level->modified_time }}</td>
                <td><a href="/level/{{ $level->level_id }}/update"><i class="bi bi-pencil btn btn-primary"></i></a></td>
                <td>
                    <button type="button" class="btn btn-danger bi-trash3-fill" data-bs-toggle="modal"
                        data-bs-target="#levelDeleteModal" data-level-id="{{ $level->level_id }}"
                        data-level-name="{{ $level->level_name }}">
                    </button>
                </td>
            </tr>
        </table>
    @endif
@endsection
