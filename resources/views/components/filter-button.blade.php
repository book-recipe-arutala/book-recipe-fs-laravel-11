<div class="d-flex justify-items-center filter-container gap-4">
    <div class="filter-btn">
        <button class="w-100 filter btn btn-light d-flex justify-content-center mx-2" type="button"
            id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
            Filter
            <i class="bi bi-filter"></i>
        </button>
        <div class="dropdown-menu p-4 w-max-xs" aria-labelledby="dropdownMenuButton1">
            <div class="row custom-fontsize-content1">
                <div class="col-md-6 mb-3">
                    <p>Tingkat Kesulitan</p>
                    <select class="form-select custom-fontsize-content2" aria-label="Level Select" name='levelId'>
                        <option {{ $isDefaultSelected() }} value=''>All</option>
                        @foreach ($levelsData as $level)
                            <option value={{ $level->level_id }} {{ $isSelected($levelId, $level->level_id) }}>
                                {{ $level->level_name }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6 mb-3">
                    <p>Kategori</p>
                    <select class="form-select custom-fontsize-content2" aria-label="Category Select" name='categoryId'>
                        <option {{ $isDefaultSelected() }} value=''>All</option>
                        @foreach ($categoriesData as $category)
                            <option value={{ $category->category_id }}
                                {{ $isSelected($categoryId, $category->category_id) }}>
                                {{ $category->category_name }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row custom-fontsize-content1">
                <div class="col-md-6 mb-3">
                    <p>Waktu Memasak</p>
                    <select class="form-select custom-fontsize-content2" aria-label="Third select example"
                        name='timeCook'>
                        <option value='' {{ $isDefaultSelected() }}>All</option>
                        <option value="0-30" {{ $isSelected($timeCook, '0-30') }}>0-30</option>
                        <option value="30-60" {{ $isSelected($timeCook, '30-60') }}>30-60</option>
                        <option value="60" {{ $isSelected($timeCook, '60') }}>> 60</option>
                    </select>
                </div>

                <div class="col mb-3 d-none d-md-block">
                    <p>Sortir</p>
                    <select class="form-select custom-fontsize-content2" aria-label="Fourth select example"
                        name='sortBy'>
                        <option value='' {{ $isDefaultSelected() }}>All</option>
                        <option value="recipeName,asc" {{ $isSelected($sortBy, 'recipeName,asc') }}>Nama Resep A-Z</option>
                        <option value="recipeName,desc" {{ $isSelected($sortBy, 'recipeName,desc') }}>Nama Resep Z-A</option>
                        <option value="timeCook,asc" {{ $isSelected($sortBy, 'timeCook,asc') }}>Waktu memasak A-Z</option>
                        <option value="timeCook,desc" {{ $isSelected($sortBy, 'timeCook,desc') }}>Waktu memasak Z-A</option>
                    </select>
                </div>

            </div>
            <div class="row mt-3 custom-fontsize-content1">
                <div class="col">
                    <button type="submit" class="btn btn-primary">Filter</button>
                </div>
            </div>
        </div>
    </div>

    {{-- <div class="dropdown filter-btn d-md-none">
        <button class="btn w-100 btn-light dropdown-toggle" type="button" id="dropdownMenuButton2"
            data-bs-toggle="dropdown" aria-expanded="false">
            Sort By
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
            <li><button class="dropdown-item" type="button" value="recipeName,asc" wire:model='sortBy'>Nama Resep
                    A-Z</button></li>
            <li><button class="dropdown-item" type="button" value="recipeName,desc" wire:model='sortBy'>Nama Resep
                    Z-A</button></li>
            <li><button class="dropdown-item" type="button" value="timeCook,asc" wire:model='sortBy'>Waktu memasak
                    A-Z</button></li>
            <li><button class="dropdown-item" type="button" value="timeCook,desc" wire:model='sortBy'>Waktu memasak
                    Z-A</button></li>
        </ul>
    </div> --}}

</div>
