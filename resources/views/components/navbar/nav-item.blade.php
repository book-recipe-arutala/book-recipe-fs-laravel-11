<li class="nav-item">
    <a class="navbar-link {{ $isActive() }}" href="{{ route($route) }}">
        {{ $text }}
    </a>
</li>
