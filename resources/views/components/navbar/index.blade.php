<nav class="navbar navbar-expand-lg navbar-custom">
    <div class="container">
        <a class="navbar-brand d-flex align-items-center" href="{{ route('recipe') }}">
            <img src="{{ asset('icon/logo.svg') }}" alt="Book-recipe" class="img-fluid me-2 navbar-logo">
            Buku Resep 79
        </a>
        <div class="d-flex flex-row justify-content-center align-items-center">

            <button class="navbar-toggler d-lg-none" type="button" data-bs-toggle="offcanvas"
                data-bs-target="#offcanvasExample" aria-controls="offcanvasExample">
                <i class="bi bi-list"></i>
            </button>

            <div class="collapse navbar-collapse">
                <ul class="navbar-nav align-items-center">
                    <x-navbar.nav-item route="recipe" text="Daftar Resep Masakan" />
                    <x-navbar.nav-item route="recipe.myrecipes" text="Resep Saya" />
                    {{-- <li class="nav-item">
                        <a class="navbar-link {{ Route::is('recipe') ? 'navbar-active' : '' }}"
                            href="{{ route('recipe') }}">Daftar Resep Masakan</a>
                    </li>
                    <li class="nav-item">
                        <a class="navbar-link {{ Route::is('recipe.myrecipes') ? 'navbar-active' : '' }}"
                            href="{{ route('recipe.myrecipes') }}">Resep Saya</a>
                    </li> --}}
                    <li class="nav-item dropdown">
                        <a class="navbar-link" href="#" role="button" data-bs-toggle="dropdown"
                            aria-expanded="false">
                            <i class="bi bi-person-circle" style="font-size: 36px;"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="#">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
