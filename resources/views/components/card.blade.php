@props(['data'])
<div class="col-lg-3 col-md-4 col-sm-6 col-12 mb-3">
    <div class="card">
        @if (Route::is('recipe.myrecipes'))
            <div class="btn-group">
                <a href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                    <i class="bi bi-three-dots" style="color: #ffffff; font-size: 24px; text-shadow: rgb(0, 0, 0) 1px 0 10px;"></i>
                </a>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <li>
                        <a class="dropdown-item text-recipe-primary" href="/recipe/{{ $data->recipe_id }}/edit">
                            <i class="bi bi-pencil-square"></i> Edit</a>
                    </li>
                    <li>
                        <a class="dropdown-item text-recipe-danger" data-bs-toggle="modal"
                            data-bs-target="#modalDeleteRecipe{{ $data['recipeId'] }}">
                            <i class="bi bi-trash-fill"></i> Hapus</a>
                    </li>
                </ul>
            </div>
        @endif

        {{-- Path langsung jadi alamat DB --}}
        {{-- @if (Str::isUrl($data->image_filename))
            <img src="{{ $data->image_filename }}" alt="{{ $data->recipe_name }}" class="img-fluid">
        @elseif ($data->image_filename != null && Storage::disk('local')->exists($data->image_filename))
            <img src="{{ Storage::url($data->image_filename) }}" alt="{{ $data->recipe_name }}" class="img-fluid">
        @else
            <img src="{{ Storage::url('recipe-images/default.png') }}" alt="{{ $data->recipe_name }}" class="img-fluid">
        @endif --}}

        {{-- Di DB hanya name saja --}}
        @if (Str::isUrl($data->image_filename))
            <img src="{{ $data->image_filename }}" alt="{{ $data->recipe_name }}" class="img-fluid">
        @elseif ($data->image_filename != null && Storage::disk('public')->exists('recipe-images/' . $data->image_filename))
            <img src="{{ Storage::url('recipe-images/' . $data->image_filename) }}" alt="{{ $data->recipe_name }}"
                class="img-fluid">
        @else
            <img src="{{ Storage::url('recipe-images/default.png') }}" alt="{{ $data->recipe_name }}" class="img-fluid">
        @endif
        <div class="card-body">
            <div class="d-flex justify-content-between mb-n1">
                <p>{{ $data->category->category_name }}</p>
                <p>{{ $data->level->level_name }}</p>
            </div>
            <h6 class="text-truncate">{{ $data->recipe_name }}</h6>
            <div class="d-flex justify-content-between mt-3">
                <p><i class="bi bi-clock"></i>&nbsp;&nbsp;{{ $data->time_cook }} Menit</p>
            </div>
            <div class="d-flex justify-content-center mt-2">
                <a href="" class="link-offset-3">Lihat Detil Resep</a>
            </div>
        </div>
    </div>
</div>
