@props(['value', 'selectedEntries'])
<button type="submit" value="{{ $value }}" name="pageSize"
    class="btn entries-button custom-fontsize-content2 {{ $selectedEntries == $value ? 'selected-entries' : '' }}">
    {{ $value }}
</button>
