@props(['totalPages', 'pageNumber'])
<div class="pagination-container">
    <button class="pagination-button" wire:click="previousPage" @if ($pageNumber == 1) disabled @endif>
        <div class=""><i class="bi bi-chevron-left"></i></i></div>
    </button>
    @foreach (range(1, $totalPages) as $page)
        <button
            class="btn pagination-button custom-fontsize-content2 {{ $page == $pageNumber ? 'selected-pagination' : '' }}"
            wire:click="changePage({{ $page }})" @if ($page == $pageNumber) disabled @endif>
            <div class="">{{ $page }}</div>
        </button>
    @endforeach
    <button class="pagination-button" wire:click="nextPage" @if ($pageNumber == $totalPages) disabled @endif>
        <div class=""><i class="bi bi-chevron-right"></i></i></div>
    </button>
</div>
