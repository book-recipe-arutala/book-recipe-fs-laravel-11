@props(['recipeName'])
<div class="input-group custom-search text-center mx-md-5">
    <span class="input-group-text">
        <i class="bi bi-search"></i>
    </span>
    <input type="text" name="recipeName" class="form-control" placeholder="Search" value="{{ $recipeName }}">
    <div class="input-group-append">
    </div>
</div>
