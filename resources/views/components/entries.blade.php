@props(['selectedEntries'])
<div class="d-flex align-items-center">
    <p class="m-0 fw-semibold me-2" style="color: #787885; font-size: 16px">Entries</p>
    <div class="entries-container">
        <x-entries.entries-button :$selectedEntries :value=8 />
        <x-entries.entries-button :$selectedEntries :value=16 />
        <x-entries.entries-button :$selectedEntries :value=48 />
    </div>
</div>
