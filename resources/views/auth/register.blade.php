@extends('layouts.auth-layout')
@section('title', 'Login - Book Recipe')

@section('content')
    <div>
        <div class="auth-header pt-1 pb-2">
            Daftar
        </div>
        <form class="auth-form" action="{{ route('register') }}" method="POST">
            @csrf
            <div class="mb-1">
                <label for="inputUsername" class="form-label">Username <span>*</span></label>
                <input type="text" class="form-control" id="inputUsername" name="username" value="{{ old('username') }}">
                @error('username')
                    <p class="text-danger m-0 p-0">{{ $message }}</p>
                @else
                    &nbsp;
                @enderror
            </div>
            <div class="mb-1">
                <label for="inputFullname" class="form-label">Nama Lengkap <span>*</span></label>
                <input type="text" class="form-control" id="inputFullname" name="fullname" value="{{ old('fullname') }}">
                @error('fullname')
                    <p class="text-danger m-0 p-0">{{ $message }}</p>
                @else
                    &nbsp;
                @enderror
            </div>
            <div class="mb-1">
                <label for="inputPassword" class="form-label">Kata Sandi <span>*</span></label>
                <div class="input-group">
                    <input type="password" class="form-control" id="inputPassword" name="password"
                        value="{{ old('password') }}">
                    <button type="button" class="btn btn-outline-secondary" id="togglePassword">
                        <i class="bi bi-eye-fill" id="toggleIcon"></i>
                    </button>
                </div>
                @error('password')
                    <p class="text-danger m-0 p-0">{{ $message }}</p>
                @else
                    &nbsp;
                @enderror
            </div>
            <div class="mb-1">
                <label for="inputConfirmPassword" class="form-label">Konfirmasi Kata Sandi <span>*</span></label>
                <div class="input-group">
                    <input type="password" class="form-control" id="inputConfirmPassword" name="confirmPassword"
                        value="{{ old('confirmPassword') }}">
                    <button type="button" class="btn btn-outline-secondary" id="toggleConfirmPassword">
                        <i class="bi bi-eye-fill" id="toggleConfirmIcon"></i>
                    </button>
                </div>
                @error('confirmPassword')
                    <p class="text-danger m-0 p-0">{{ $message }}</p>
                @else
                    &nbsp;
                @enderror
            </div>
            <div class="d-grid mt-2">
                <button type="submit" class="btn btn-custom">Daftar</button>
            </div>
            <div class="container text-center mt-4 auth-footer">
                <div class="col-auto">
                    <a href="{{ route('login') }}">Batal, Kembali ke Halaman Login</a>
                </div>
            </div>
        </form>
    </div>
@endsection
