@extends('layouts.auth-layout')
@section('title', 'Login - Book Recipe')

@section('content')
    <div>
        <div class="auth-header pt-1 pb-2">
            Login
        </div>
        <form class="auth-form" action="{{ route('login') }}" method="POST">
            @csrf
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Username <span class="text-danger"> *</span></label>
                <input type="text" class="form-control" name="username">
                @error('username')
                    <p class="text-danger m-0 p-0">{{ $message }}</p>
                @enderror
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Kata Sandi <span class="text-danger"> *</span></label>
                <input type="password" class="form-control" name="password">
                @error('password')
                    <p class="text-danger m-0 p-0">{{ $message }}</p>
                @enderror
            </div>
            <div class="d-grid">
                <button type="submit" class="btn btn-custom">Login</button>
            </div>
            <div class="container text-center mt-4 auth-footer">
                <div class="col-auto">
                    <p>Belum Punya Akun? <a href="/register">Daftar Disini</a></p>
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <a href="">About</a>
                            </div>
                            <div class="col">
                                <a href="">Contact</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="modal fade" id="errorModal" tabindex="-1" aria-labelledby="errorModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body mb-4">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-1 ms-auto">
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                        </div>
                        <div class="row text-center">
                            <i class="bi bi-x-circle text-danger" style="font-size: 80px"></i>
                            <p style="font-size: 32px" class="fw-bold text-danger">Error</p>
                            @error('authError')
                                <p style="color: #263238; font-size: 18px"> {{ $message }} </p>
                            @enderror
                            <div class="col">
                                <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Continue</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
