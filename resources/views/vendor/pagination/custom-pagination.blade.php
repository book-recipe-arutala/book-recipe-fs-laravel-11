@if ($paginator->hasPages()) {{-- Pengecekan apakah halaman punya pagination --}}
    <div class="pagination-container">
        {{-- Previous Button --}}
        @if ($paginator->onFirstPage()) {{-- Kondisi previous button jika berada di halaman pertama --}}
            <button class="pagination-button" disabled>
                <div class=""><i class="bi bi-chevron-left"></i></div>
            </button>
        @else {{-- Kondisi previous button jika tidak berada di halaman pertama --}}
            <a href="{{ $paginator->previousPageUrl() }}">
                <button class="pagination-button">
                    <div class=""><i class="bi bi-chevron-left"></i></div>
                </button>
            </a>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage()) {{-- Kondisi nomor halaman yang ditempati --}}
                        <button class="btn pagination-button custom-fontsize-content2 selected-pagination" disabled>
                            <div class="">{{ $page }}</div>
                        </button>
                    @else {{-- Kondisi nomor halaman lain yang ditempati --}}
                        <a href="{{ $url }}">
                            <button class="btn pagination-button custom-fontsize-content2">
                                <div class="">{{ $page }}</div>
                            </button>
                            </li>
                        </a>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Button --}}
        @if ($paginator->hasMorePages()) {{-- Kondisi next button jika tidak berada di akhir halaman --}}
            <a href="{{ $paginator->nextPageUrl() }}">
                <button class="pagination-button">
                    <div class=""><i class="bi bi-chevron-right"></i></div>
                </button>
            </a>
        @else {{-- Kondisi next button jika berada di akhir halaman --}}
            <button class="pagination-button" disabled>
                <div class=""><i class="bi bi-chevron-right"></i></div>
            </button>
        @endif
    </div>
@endif
