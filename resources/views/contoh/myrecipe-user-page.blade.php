@extends('layouts.master')

@section('title', 'My Recipe - User')
@section('content')
    <div>
        <h3>Halaman My Recipe!</h3>
        <h3>Nama user: {{$fullname}}</h3>
        <h3>Username: {{$username}}</h3>

        <form action="/logout" method="POST">
            @csrf
            <button type="submit" class="btn btn-danger">Button Log out</button>
        </form>
    </div>
@endsection
