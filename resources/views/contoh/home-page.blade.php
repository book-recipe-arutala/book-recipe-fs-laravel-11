@extends('layouts.master-contoh')
@section('judulHalaman', 'Home Page Laravel')

@section('content')
    <div>
        <h1>Selamat datang di web kami!</h1>
    </div>
    <div class="row">
        <div class="col-4">
            <div class="card" style="width: 100%;">
                <img src="https://wowslider.com/sliders/demo-93/data1/images/lake.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                        card's content.</p>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card" style="width: 100%;">
                <img src="https://wowslider.com/sliders/demo-93/data1/images/sunset.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                        card's content.</p>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card" style="width: 100%;">
                <img src="https://wowslider.com/sliders/demo-23/data1/images/squantzpond209864.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                        card's content.</p>
                </div>
            </div>
        </div>
    </div>
@endsection
