@extends('layouts.master')

@section('title', 'Dashboard User')
@section('content')
    <div>
        <h3>Selamat! Anda sudah login!</h3>
        {{-- Data langsung diambil dari view --}}
        {{-- <h3>Nama user: {{Auth::user()->fullname}}</h3>
        <h3>Username: {{Auth::user()->username}}</h3> --}}
        {{-- Data diterima dari controller --}}
        <h3>Nama user: {{$fullname}}</h3>
        <h3>Username: {{$username}}</h3>

        <form action="/logout" method="POST">
            @csrf
            <button type="submit" class="btn btn-danger">Button Log out</button>
        </form>
    </div>
@endsection
