@extends('layouts.master-contoh')
@section('judulHalaman', 'Data List Laravel')

@section('content')
    <div>
        <h1>Anda bisa lihat data-data list di bawah ini</h1>
        <ul>
            <li>Coffee</li>
            <li>Tea</li>
            <li>Milk</li>
        </ul>
    </div>
@endsection
