<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>
        @yield('title')
    </title>
    {{-- Bootstrap CSS --}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
    {{-- Local CSS --}}
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/auth.css') }}">
</head>

<body>
    {{-- @if ($errors->any())
        <div class="alert alert-danger alert-dismissible fade show fixed-alert" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            <p>{{ $errors->first() }}</p>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif --}}

    {{-- @if ($errors->has('error'))
        <div class="alert alert-danger alert-dismissible fade show fixed-alert" role="alert">
            <p>{{ $errors->first() }}</p>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif --}}

    @error('error')
        <div class="alert alert-danger alert-dismissible fade show fixed-alert" role="alert">
            <p>{{ $message }}</p>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @enderror

    @if (session('success'))
        <div class="alert alert-success fixed-alert" role="alert">
            {{ session('success') }}
        </div>
    @endif

    <div class="d-flex justify-content-center" style="padding-top: 64px; padding-bottom: 64px">
        <div>
            <div class="auth-title text-center">
                <p>Buku Resep 79</p>
                <img src="{{ asset('icon/logo.svg') }}" alt="Book-recipe" class="text-center" height="80px"
                    style="margin-top: -12px; margin-bottom: 32px">
            </div>
            <div class="auth-form-box">
                @yield('content')
            </div>
        </div>
    </div>
    {{-- Script --}}
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"
        integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous">
    </script>

    <script>
        // JQuery
        $(document).ready(function() {
            // Auto hide alert
            window.setTimeout(function() {
                $(".alert").fadeTo(500, 0).slideUp(500, function() {
                    $(this).remove();
                });
            }, 4000);

            // Button toggle
            $('#togglePassword').click(function() {
                const passwordInput = $('#inputPassword');
                const type = passwordInput.attr('type') === 'password' ? 'text' : 'password';
                passwordInput.attr('type', type);

                const toggleIcon = $('#toggleIcon');
                toggleIcon.toggleClass('bi-eye-fill bi-eye-slash-fill');
            });

            $('#toggleConfirmPassword').click(function() {
                const confirmPasswordInput = $('#inputConfirmPassword');
                const type = confirmPasswordInput.attr('type') === 'password' ? 'text' : 'password';
                confirmPasswordInput.attr('type', type);

                const toggleConfirmIcon = $('#toggleConfirmIcon');
                toggleConfirmIcon.toggleClass('bi-eye bi-eye-slash');
            });

            // Show Modal
            @error('authError')
                $('#errorModal').modal('show');
            @enderror
        });
    </script>
</body>

</html>
