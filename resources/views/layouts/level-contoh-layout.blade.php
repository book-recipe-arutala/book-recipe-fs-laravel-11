<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    {{-- Bootstrap CSS --}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <style>
        table,
        th,
        td {
            border: 1px solid black;
            border-collapse: collapse;
            text-align: center;
        }

        .fixed-alert {
            position: fixed;
            bottom: 20px;
            right: 20px;
            z-index: 1050;
            width: auto;
            max-width: 300px;
        }
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-lg bg-primary mb-5" data-bs-theme="dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="/level">Level Data</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link {{ request()->is('level') ? 'active fw-bold' : '' }}" aria-current="page"
                            href="/level">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Route::currentRouteName() == 'levels.create' ? 'active fw-bold' : '' }}"
                            href="{{ route('levels.create') }}">Create Level</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        @if (session('success'))
            <div class="alert alert-success fixed-alert" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="text-center mb-3 fw-bold fs-4">
            @yield('title')
        </div>
        @yield('content')
    </div>

    <script>
        $(document).ready(function() {
            $('#levelDeleteModal').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget);
                var levelId = button.data('level-id');
                var levelName = button.data('level-name');

                var modal = $(this);
                modal.find('.modal-body #level-name').text(levelName);
                modal.find('#delete-form').attr('action', '/level/' + levelId);
            });
        });
        $(document).ready(function() {
            window.setTimeout(function() {
                $(".alert").fadeTo(500, 0).slideUp(500, function() {
                    $(this).remove();
                });
            }, 4000);
        });
    </script>

    {{-- Bootstrap JS --}}
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous">
    </script>
</body>

</html>
